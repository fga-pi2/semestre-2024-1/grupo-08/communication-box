# Levantamento de normas técnicas relacionadas ao problema

* **NBR ISO 9001:2015 - Sistema de Gestão da Qualidade - Requisitos**

<p style="text-align:justify;"> Esta norma especifica requisitos para um sistema de gestão da qualidade quando uma organização: </p>

- Necessita demonstrar sua capacidade para prover consistentemente produtos e serviços que atendam aos requisitos do cliente e aos requisitos estatutários e regulamentares aplicáveis, e 
- Visa aumentar a satisfação do cliente por meio da aplicação eficaz do sistema, incluindo processos para melhoria do sistema e para a garantia da conformidade com os requisitos do cliente e com os requisitos estatutários e regulamentares aplicáveis.

* **NBR 5410:2005 - Instalações elétricas de baixa tensão**

<p style="text-align:justify;"> Esta norma estabelece as condições a que devem satisfazer as instalações elétricas de baixa tensão, a fim de garantir a segurança de pessoas e animais, o funcionamento adequado da instalação e a conservação dos bens. Esta norma aplica-se principalmente as instalações elétricas de edificações, qualquer que seja seu uso (residencial, comercial, público, industrial, de serviços, agropecuário, hortigranjeiro, etc.), incluindo as pré-fabricadas. </p>

* **NBR 16404:2015 - Bateria chumbo-ácida estacionária ventilada – Requisitos de instalação e montagem.**

<p style="text-align:justify;"> Essa norma estabelece os requisitos para o projeto de instalação, montagem e carga inicial de bateria chumbo ácida ventilada para aplicações estacionárias, que permite reposição de água. Deve-se levar em conta alguns requisitos de segurança, como as precauções que devem ser previstas no projeto de instalação e na instalação dos elementos ou monoblocos. </p>

* **NR 10 - Segurança em instalações e serviços em eletricidade.**

<p style="text-align:justify;"> Esta norma regulamentadora estabelece os requisitos e condições mínimas objetivando a implementação de medidas de controle e sistemas preventivos, de forma a garantir a segurança e a saúde dos trabalhadores que, direta ou indiretamente, interajam em instalações elétricas e serviços com eletricidade. </p>

* **ABNT NBR 08403: Aplicação de linhas em desenhos.**

<p style="text-align:justify;"> Esta norma estabelece os padrões para a aplicação de linhas em desenhos técnicos. Define os tipos de linhas a serem utilizados para representar diferentes elementos, como contornos, cortes, eixos, entre outros. Também aborda questões como espessura e estilo das linhas, garantindo a clareza e a uniformidade nos desenhos técnicos. </p>


* **ABNT NBR 10067 - Princípios Gerais de Representação em Desenho Técnico.**

<p style="text-align:justify;"> Esta norma estabelece os princípios gerais para a representação de objetos em desenhos técnicos. Define os tipos de projeções (ortográfica, isométrica, etc.), convenções de representação (vistas, cortes, seções), e critérios para a disposição dos elementos em uma folha de desenho. Seu objetivo é garantir a precisão, clareza e interpretação correta dos desenhos técnicos. </p>


* **ABNT NBR 10068 - Folha de Desenho.**

<p style="text-align:justify;"> Esta norma especifica as características das folhas de desenho a serem utilizadas em desenhos técnicos. Define dimensões, margens, formatos, tipos de papel e informações a serem incluídas no cabeçalho das folhas de desenho. Isso assegura a padronização e a interoperabilidade entre diferentes desenhos técnicos, facilitando sua interpretação e manuseio. </p>


* **ABNT NBR 10126 - Cotagem em Desenho Técnico.**

<p style="text-align:justify;"> Esta norma estabelece os princípios e as convenções para a realização de cotagens em desenhos técnicos. Define os tipos de cotas (linear, angular, etc.), suas representações gráficas, e critérios para sua aplicação e dimensionamento. Seu objetivo é garantir a precisão e a clareza na indicação das dimensões dos objetos representados nos desenhos técnicos, facilitando sua compreensão e fabricação. </p>


**Referências em ABNT:**

* ABNT NBR ISO 9001:2008. Sistemas de gestão da qualidade - requisitos. 
* ABNT NBR 5410:2004. Instalações elétricas de baixa tensão.
* ABNT NBR 16404:2015. Bateria chumbo-ácida estacionária ventilada: Requisitos de instalação e montagem.
* NR 10: Segurança em instalações e serviços em eletricidade. 2004.
* ABNT NBR 08403: Aplicação de linhas em desenhos - Tipos de linhas - Larguras das linhas Rio de Janeiro, 1984.
* ABNT NBR 10067 - Princípios Gerais de Representação em Desenho Técnico, 1995
* ABNT NBR 10068 - Folha de Desenho - Layout e Dimensões, 1987
* ABNT NBR 10126 - Cotagem em Desenho Técnico, 1987

## Histórico de Revisão

| Data | Versão| Descrição | Autor |
| ------ |----|----|----|
| 24/04/2024 | 1.0 | Normas de energia | Carol |
| 27/04/2024 | 2.0 | Normas de Software | Ciro |
| 03/04/2024 | 3.0 | Revisão de documento  | Ana |
| 04/04/2024 | 4.0 | Correções de erros de formatação | Ana |
| 08/07/2024 | 5.0 | Correções de erros de português | Ana |
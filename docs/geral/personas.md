# Personas

## Introdução à Definição de Personas

<p style="text-align: justify;">
A definição de personas é uma técnica utilizada no design centrado no usuário para criar representações fictícias e detalhadas de usuários típicos de um produto ou serviço. Essas representações ajudam a entender as necessidades, comportamentos, objetivos e desafios dos usuários, permitindo que os desenvolvedores e designers criem soluções mais eficazes e personalizadas. As personas são baseadas em dados reais obtidos por meio de pesquisa de mercado, entrevistas, observações e outras fontes de informação, garantindo que reflitam os perfis dos usuários de maneira precisa e realista.
</p>

## Personas para o Projeto ScanPoint

### Persona 1: João, o Estudante Universitário

#### Dados Demográficos
- **Nome:** João Pereira
- **Idade:** 22 anos
- **Gênero:** Masculino
- **Localização:** Brasília, DF
- **Educação:** Estudante de Engenharia Mecatrônica na Universidade de Brasília (UnB)
- **Renda:** Baixa, depende de bolsa de estudos e apoio financeiro da família

#### Dados Psicográficos
- **Interesses:** Tecnologia, robótica, impressão 3D, inovação
- **Hobbies:** Participa de competições de robótica, maker spaces, leitura de revistas de tecnologia
- **Personalidade:** Curioso, autodidata, inovador, colaborativo
- **Valores:** Valorização do conhecimento, sustentabilidade, colaboração

#### Objetivos
- **Curto Prazo:** Concluir projetos acadêmicos utilizando ferramentas de digitalização 3D
- **Longo Prazo:** Trabalhar em uma empresa de tecnologia ou abrir sua própria startup de robótica

#### Desafios
- **Financeiros:** Limitações financeiras para adquirir equipamentos caros
- **Acadêmicos:** Necessidade de concluir projetos com prazos apertados
- **Tecnológicos:** Manter-se atualizado com as tecnologias mais recentes

#### Citações
- **"Eu preciso de ferramentas acessíveis que me ajudem a transformar minhas ideias em projetos reais sem gastar muito."**
- **"A possibilidade de criar modelos 3D a partir de objetos físicos pode revolucionar meus projetos acadêmicos."**

### Persona 2: Ana, a Designer de Produtos

#### Dados Demográficos
- **Nome:** Ana Costa
- **Idade:** 35 anos
- **Gênero:** Feminino
- **Localização:** São Paulo, SP
- **Educação:** Graduação em Design de Produtos
- **Renda:** Média alta

#### Dados Psicográficos
- **Interesses:** Design industrial, inovação, impressão 3D, sustentabilidade
- **Hobbies:** Criação de protótipos, exposições de design, leitura de blogs de design
- **Personalidade:** Criativa, detalhista, inovadora, prática
- **Valores:** Estética, funcionalidade, sustentabilidade

#### Objetivos
- **Curto Prazo:** Desenvolver protótipos precisos e inovadores para seus clientes
- **Longo Prazo:** Tornar-se uma referência em design sustentável e impressão 3D

#### Desafios
- **Tempo:** Necessidade de criar protótipos rapidamente para atender prazos de clientes
- **Precisão:** Alta demanda por precisão nos modelos 3D
- **Concorrência:** Manter-se competitiva em um mercado saturado

#### Citações
- **"A precisão e a rapidez no desenvolvimento de protótipos são cruciais para o meu trabalho."**
- **"Ferramentas que combinam tecnologia de ponta com facilidade de uso são essenciais para meu processo criativo."**

### Persona 3: Carlos, o Empreendedor de uma pequena empresa

#### Dados Demográficos
- **Nome:** Carlos Oliveira
- **Idade:** 45 anos
- **Gênero:** Masculino
- **Localização:** Curitiba, PR
- **Educação:** MBA em Gestão de Negócios
- **Renda:** Média

#### Dados Psicográficos
- **Interesses:** Inovação empresarial, tecnologia, manufatura, prototipagem
- **Hobbies:** Participação em feiras de tecnologia, leitura sobre inovação empresarial, networking
- **Personalidade:** Visionário, prático, focado em resultados, líder
- **Valores:** Eficiência, inovação, crescimento empresarial

#### Objetivos
- **Curto Prazo:** Implementar tecnologias de digitalização 3D na produção de sua empresa
- **Longo Prazo:** Expandir o negócio e aumentar a eficiência operacional

#### Desafios
- **Orçamento:** Necessidade de soluções que se ajustem ao orçamento da empresa
- **Implementação:** Treinar a equipe para usar novas tecnologias
- **Competitividade:** Manter-se à frente da concorrência através da inovação

#### Citações
- **"Precisamos de soluções que aumentem a nossa eficiência sem extrapolar nosso orçamento."**
- **"A digitalização 3D pode ser um diferencial competitivo para nossa empresa."**

## Conclusão

<p style="text-align: justify;">
A definição de personas é um passo crucial no desenvolvimento de produtos e serviços centrados no usuário. Para o projeto ScanPoint, identificamos três personas principais que representam diferentes segmentos de nosso público-alvo: estudantes universitários, designers de produtos e empreendedores de pequenas empresas. No entanto, à medida que o projeto cresce e evolui, a diversidade de personas também pode se expandir para incluir novos setores e perfis de usuários.
<br/><br/>
Por ser um projeto abrangente, o ScanPoint tem o potencial de atender a uma ampla gama de usuários, desde profissionais da saúde e museus até indústrias de entretenimento e jogos. Com o crescimento do projeto, novas personas podem ser desenvolvidas para refletir as necessidades e características de diferentes mercados e contextos, garantindo que a solução permaneça relevante e eficaz para todos os seus usuários. A adaptação contínua às demandas emergentes e a inclusão de feedback dos usuários serão fundamentais para o sucesso e a expansão do ScanPoint.
</p>


## Referências

> **SEBRAE** - Definição de Personas. Disponível em: [SEBRAE](https://sebrae.com.br/sites/PortalSebrae/artigos/defina-a-persona-e-construa-uma-relacao-ideal-com-seu-publico,63c51171d1561810VgnVCM100000d701210aRCRD)

> **MJV Innovation** - Entenda o conceito de personas, e o seu cliente ideal. Disponível em: [MJV Innovation](https://www.mjvinnovation.com/pt-br/blog/personas-uma-ferramenta-poderosa-no-design-thinking-2/)

> **ZENDESK** - O que é uma persona ? Como criar uma ? Disponível em: [ZENDESK](https://www.zendesk.com.br/blog/criacao-de-persona/)

## Histórico de Revisão

| Versão | Data | Descrição | Autor |
|----|----|----|----|
| 1.0 | 20/05/2024 | Criação de Documento de Personas | [Vinicius Vieira](https://gitlab.com/viniciusvieira00) |
# Cronograma

<p style="text-align: justify;"> Visando garantir que o projeto ocorra como o esperado, foi criado o cronograma do projeto, contendo as atividades a serem desenvolvidas,a quem está designada, o tempo de duração, o início e a conclusão programada. As atividades foram planejadas para que haja um tempo de teste e correção antes de cada ponto de controle. </p>

|ID|Atividade|Duração|Início|Fim|Atribuído|
|--|---------|-------|------|---|---------|
|01| Definição de temas e grupos |06 dias | 22/03/2024 | 27/03/2024|Todas|
|02| Definição de papéis  e objetivos|06 dias|29/03/2024|03/04/2024|Todas|
|03| Levantamento de requisitos|06 dias|05/04/2024|10/04/2024|Todas|
|04| Documentação dos requisitos funcionais e não funcionais | 07 dias |06/04/2024|12/04/2024| Brenda e Carol|
|05| Termo de Abertura do Projeto (TAP)| 07 dias |06/04/2024|12/04/2024| Maria e Miguel|
|06| Estrutura Analítica de Projeto (EAP)| 07 dias |06/04/2024|12/04/2024| Ana Carolina e Denniel|
|07|Definição, sequenciamento e cronograma de atividades| 07 dias |06/04/2024|12/04/2024| Carla|
|08| Estimativa de custos | 07 dias |06/04/2024|12/04/2024| Artur e Cássio|
|09| Levantamento de riscos e contingência para a execução do projeto e avaliação do impacto | 07 dias |06/04/2024|12/04/2024| Ciro, Diogo e Lucas|
|10|Diagrama de caso de uso| 07 dias |06/04/2024|12/04/2024| Guilherme, Pedro e Vinicius |
|11|Desenhos mecânicos| 07 dias |13/04/2024| 19/04/2024| Cássio, Diogo e Maria |
|12|Diagramas unifilares/trifilares de sistemas de alimentação| 07 dias |13/04/2024|19/04/2024| Carol e Lucas|
|13|Diagramas esquemáticos de circuitos eletrônicos| 07 dias |13/04/2024|19/04/2024| Miguel|
|14|Diagramas detalhando barramentos de alimentação dos circuitos eletrônicos| 07 dias |20/04/2024|/26/2024| Miguel| 
|15|Diagramas com detalhes de lógicas e protocolos de comunicação entre elementos| 07 dias |13/04/2024|19/04/2024| Miguel | 
|16|Diagramas com protocolos de comunicação entre componentes do software| 07 dias |13/04/2024|19/04/2024| Denniel e Ciro|
|17| Diagrama de arquitetura de software | 07 dias | 13/04/2024| 19/04/2024|Artur, Pedro e Guilherme|
|18| Diagrama de pacotes|07 dias| 13/04/2024| 19/04/2024|Vinicius|
|20| Documento de visão |07 dias|13/07/2024|19/04/2024|Brenda, Carla e Ana |
|21| Revisão e ajuste dos  artefatos previstos na fase 2 |03 dias|20/04/2024|22/04/2024||
|22| Protótipo de alta fidelidade de software e documento de identidade|07 dias|23/04/2024| 29/04/2024||
|23| Escrita do relatório de ponto de controle|07 dias| 23/04/2024 |29/04/2024|Todos|
|24| Aquisição dos materiais |17 dias|29/04/2024| 15/05/2024| cada sub grupo|
|25| Revisão do relatório de ponto de controle 1|02 dias|29/04/2024 | 30/04/2024| Gerência |
|26| Ajustes do relatório de ponto de controle 1|03 dias|30/04/2024 | 02/05/2024| Todos |
|27| Entrega do relatório de ponto de controle 1|01 dia| 03/05/2024| 03/05/2024 | Gerência|
|28| Elaboração da apresentação| 03 dias| 04/05/2024 | 07/05/2024| Todos|
|29| Apresentação 1 |01 dia|08/05/2024| 08/05/2024|Todos|
|30| Desenvolvimento do software |23 dias|09/05/2024 | 31/05/2024| Equipe de software|
|31| Cálculo estrutural| 07 dias |09/05/2024| 15/05/2024 |Equipe de estrutura|
|32| Construção da estrutura |20 dias|16/05/2024 |04/06/2024 |Equipe de Estrutura|
|33| Construção do subsistema de energia| 23 dias |09/05/2024|31/05/2024|Equipe de energia|
|34| Construção do subsistema de eletrônica|23 dias|09/05/2024|31/05/2024|Miguel|
|35| Teste do software |05 dias|01/06/2024|05/06/2024|Software| 
|36| Teste da estrutura|02 dias|05/06/2024|07/06/2024|Estrutura|
|37| Teste do subsistema de energia e eletrônica|05 dias|01/06/2024|05/06/2024|Equipe energia e eletrônica|
|38| Avaliar e homologar resultados|02 dias|05/06/2024|06/06/2024|Diretores técnicos|
|39| Escrita do relatório de ponto de controle|07 dias| 26/05/2024 |01/06/2024|Todos|
|40| Revisão do relatório de ponto de controle|02 dias|02/06/2024 | 03/06/2024| Gerência|
|41| Ajustes do relatório de ponto de controle |03 dias|04/06/2024 | 06/06/2024| Todos|
|42| Entrega do relatório de ponto de controle |01 dia| 07/06/2024| 07/06/2024 | Gerência|
|43| Elaboração da apresentação| 04 dias| 08/06/2024 | 11/06/2024| Todos|
|44| Apresentação 2 |01 dia|12/06/2024| 12/06/2024|Todos|
|45| Integração entre sistemas |09 dias|13/06/2024| 21/06/2024| Todos|
|46| Testar o produto final  |04 dias| 22/06/2024 | 25/06/2024|Todos|
|47| Ajustes necessários|05 dias|26/06/2024 |30/06/2024|Todos|
|48| Avaliar e homologar o produto final |02 dias |01/07/2024| 02/07/2024|Todos|
|49| Elaboração da apresentação (banner e vídeo)| 02 dias| 01/07/2024 | 02/07/2024| Todos|
|50| Apresentação 2 |01 dia|03/07/2024| 03/07/2024|Todos|
|51| FIT |01 dia| 10/07/2024| 10/07/2014|Todos|

<font size="2"><p style="text-align: center">Fonte: Autoria Própria.</p></font>

## Tabela de versionamento

| Versão | Versão | Descrição |Responsável|
|---|------|----------|-----------|
| 1.0 | 12/04/2024 |Criação do cronograma de atividades |Carla Rocha Cangussú|
| 2.0 | 12/04/2024 | Conclusão do cronograma | Carla Rocha Cangussú|
| 3.0 | 19/04/2024 | Ajuste de formatação | Carla Rocha Cangussú |
| 4.0 | 04/05/2024 | Ajuste de formatação e tabela de versionamento | Ana |
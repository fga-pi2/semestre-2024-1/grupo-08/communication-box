# Projeto do Subsistema de Energia

<p style="text-align:justify;">
O subsistema de energia é responsável pela alimentação dos demais subsistemas, garantindo eficiência energética e capacidade de operação. Portanto, o foco da equipe consistiu em dimensionar os gastos energéticos, selecionar componentes eficazes, de baixo consumo e implementar estratégias de gerenciamento de energia em conformidade com as normas de segurança aplicáveis, assegurando a proteção dos usuários e a conformidade com os padrões de qualidade e regulamentações pertinentes.
</p>

## 1. Levantamento de carga
<p style="text-align:justify;">
Com o intuito de determinar as características energéticas do projeto, realizou-se uma análise do consumo estimado dos dispositivos. Para uma margem de segurança foi adicionado  20%  sobre a potência total do projeto. Assim, temos o levantamento:
</p>

<font size="2"><p style="text-align: center">Tabela 1: Levantamento de carga.</p></font>


| Componente               | Qtd | Corrente [A]|Tensão [V] |Potência [W]|
| ------                   | --- | ----------- | --------- | ---------- |
|Driver A4988              |  2  |  2,00       | 12,00     | 48,00      |
|Motor                     |  2  |  0,570      | 6,80      | 7,752      |
|Potência Total [W]        |     |             |           | 55,752     |
|Potência Total com 20% [W]|     |             |           | 66,902     |

## 2. Bateria
<p style="text-align:justify;">
Para o projeto ScanPoint, está estimado em utilizar duas baterias de  12 V de 7 Ah, cada.
</p>

![Bateria](../assets/eletronica-energia/bateria_unipower.jpg)

<font size="2"><p style="text-align: center">Figura 1 - Bateria UP1270SEG.</p></font>

<font size="2"><p style="text-align: center">Tabela 2 - Caracteríticas da bateria.</p></font>


| Marca                             | UNICOBA       |
|------                             | ------        |
|    Modelo                         |UP 1270 SEG    |
|Composição das células da bateria  |Chumbo - Ácido |
|Voltagem                           | 12 V          |
|    Amperagem                      |    7 Ah       |
|    Dimensões [cm]                 |   8 x 16 x 11 |
|    Peso [kg]                      |    2          |

<p style="text-align:justify;">
Nesse cenário o projeto apresenta uma autonomia de 2 horas e 48 minutos. Apos alguns testes, foi possivel constatar que o tempo de autonomia é suficiente, pois cada escaneamento leva em torno de 40 minutos, variando de acordo com a altura do objeto. Ou seja, a autonomia fornecida cobre em torno de 3 a 4 ciclos de escaneamento completos.
</p>

<p style="text-align:justify;">
Em relacao as baterias, foram configuradas em paralelo visto que na configuração em série, a tensão sofreria um aumento, pois os componentes do projeto apresentam uma tensão de alimentação inferior ou igual a 12 V.
</p>


## 3. Cabos
<p style="text-align:justify;">
A corrente elétrica solicitada pelo sistema é de  2,41 A. A NBR 5410 foi consultada a fim de levar em consideração alguns fatores para o dimensionamento dos cabos elétricos.
</p>

![Tabela33](../assets/eletronica-energia/Tabela33.jpg)

<font size="2"><p style="text-align: center">Figura 2 - Tipos de linhas elétricas.<font size="2"><p style="text-align: center">Fonte: [NBR 2410, 2004](https://edisciplinas.usp.br/pluginfile.php/5810747/mod_resource/content/1/NBR5410%20-%20Instala%C3%A7%C3%B5es%20el%C3%A9tricas%20de%20baixa%20tens%C3%A3o.pdf).</p></font></p></font>

<p style="text-align:justify;">
Ao consultar a Figura 2 foi considerado o Método de Referência B1.
</p>

![Tabela40](../assets/eletronica-energia/Tabela40.jpg)

<font size="2"><p style="text-align: center">Figura 3 - Fatores de correção para a temperatura.<font size="2"><p style="text-align: center">Fonte: [NBR 2410, 2004](https://edisciplinas.usp.br/pluginfile.php/5810747/mod_resource/content/1/NBR5410%20-%20Instala%C3%A7%C3%B5es%20el%C3%A9tricas%20de%20baixa%20tens%C3%A3o.pdf).</p></font></p></font>
 

<p style="text-align:justify;">
Segundo o INMET, as temperaturas no Distrito Federal para o mês de julho ficam entre 20 °C e 25 °C. Portanto, considerando uma temperatura ambiente de 25 °C para uma isolação de PVC, seu fator de correção, previsto na Figura 3, será de 1,06.
</p>

![Tabela42](../assets/eletronica-energia/Tabela42.jpg)

<font size="2"><p style="text-align: center">Figura 4 - Fatores de correção aplicáveis a condutores. <font size="2"><p style="text-align: center">Fonte: [NBR 2410, 2004](https://edisciplinas.usp.br/pluginfile.php/5810747/mod_resource/content/1/NBR5410%20-%20Instala%C3%A7%C3%B5es%20el%C3%A9tricas%20de%20baixa%20tens%C3%A3o.pdf).</p></font></p></font>


<p style="text-align:justify;">
Considerando 1 para o número de circuitos e B1 como o método, segundo a Figura 4, o fator será 1.
</p>

<p style="text-align:justify;">
Após a determinação dos fatores diante de um cenário que o projeto se encontrará, será feito os cálculos para corrigir a corrente elétrica em função desses fatores.
</p>

I_corrigida = I_sistema / (F_temperatura × F_agrupameto)  [I]

Onde:
- I_corrigida: corrente elétrica corrigida;
- I_sistema: corrente elétrica do sistema;
- F_temperatura: fator de temperatura ambiente;
- F_agrupamento: fator de agrupamento do circuito.

Substituídos em [I] os valores determinados anteriormente:

I_corrigida = 2,41 / {(0,87×1) = 2,77 A              [I]

<p style="text-align:justify;">
Assim, tem-se uma corrente corrigida de 2,77 A. A Figura 5 foi analisada para determinar o cabo mais especificado para a corrente elétrica corrigida.
</p>


![Tabela37](../assets/eletronica-energia/Tabela37.jpg)

<font size="2"><p style="text-align: center">Figura 5 - Capacidade de condução de corrente. <font size="2"><p style="text-align: center">Fonte: [NBR 2410, 2004](https://edisciplinas.usp.br/pluginfile.php/5810747/mod_resource/content/1/NBR5410%20-%20Instala%C3%A7%C3%B5es%20el%C3%A9tricas%20de%20baixa%20tens%C3%A3o.pdf).</p></font></p></font>

<p style="text-align:justify;">
Baseado na corrente corrigida de 2,77 A uma seção nominal de 0,5 mm² serviria, entretanto, a própria norma determina que para circuitos de força a seção nominal mínima deva ser de 2,5 mm². Assim sendo, a seção nominal selecionada é de 2,5 mm².
</p>

## 4. Barramento

<p style="text-align:justify;">
Com a finalidade de ramificar as ligações e assim alimentar os dispositivos do projeto, são utilizados dois barramentos de 80 mm. Um barramento é utilizado para as ligações com a fiação positiva, identificada com cabos de coloração avermelhada, e o outro para a fiação negativa, identificado com cabos da cor preta.
</p>

![barramento](../assets/eletronica-energia/BARRAMENTO.jpg)

<font size="2"><p style="text-align: center">Figura 6 - Barramento 80 mm.</p></font>

## 5. Plugues P4

<p style="text-align:justify;">
Os plugues P4 macho e fêmea são dispositivos elétricos amplamente utilizados para conectar fontes de alimentação a diversos equipamentos, como câmeras de segurança, fitas de LED, placas Arduino, entre outros. No contexto da adaptação descrita, o plugue P4 macho foi instalado na ponta do carregador, enquanto o plugue P4 fêmea foi fixado na estrutura para facilitar o procedimento de conexãopara o carregamento das baterias.
</p>

Configuração de Pinos:
- O plugue P4 macho possui um pino central (positivo) e um anel externo (negativo), com dimensões de 2,1 x 5,5 mm.
- O plugue P4 fêmea possui um orifício central e um anel interno que se encaixam no plugue P4 macho. Essa configuração permite uma conexão segura e eficiente entre a fonte de alimentação e as baterias.

Tensão e Corrente:
- Embora os plugues P4 sejam projetados principalmente para transferir sinais de áudio e vídeo, no caso descrito, eles são utilizados para fornecer energia. 

Material e Construção:
- Os plugues P4 são geralmente fabricados com materiais duráveis, como plástico de alta qualidade ou metais, proporcionando resistência e proteção adequadas às conexões.
- Esses materiais garantem uma conexão estável e segura, essencial para a longevidade e confiabilidade do sistema de carregamento.

![Plugues P4](../assets/eletronica-energia/pinosp4.png)

<font size="2"><p style="text-align: center">Figura 7 - Plugues P4 macho (direita) e fêmea (esquerda).</p></font>

<p style="text-align:justify;">
Os plugues P4 macho e fêmea, mostrados na Figura 7 são componentes utilizados na adaptação realizada para o carregamento de baterias. Eles oferecem uma solução prática e confiável, garantindo uma conexão segura e eficiente entre a fonte de alimentação e as baterias. Utilizar plugues P4 de qualidade e verificar a compatibilidade entre os dispositivos são medidas essenciais para assegurar um funcionamento seguro e eficiente do sistema de carregamento.
</p>

## 6. Carregador da Bateria

<p style="text-align:justify;">
Utilizar um carregador adequado visa garantir a longevidade e o desempenho da bateria. Um carregador que fornece uma corrente inadequada pode comprometer a bateria ocasionando danos, reduzindo a vida útil da bateria ou não carregá-la de maneira eficiente. Os carregadores modernos geralmente seguem normas específicas de segurança, como a ABNT NBR IEC 60335-2-29, que garantem que os dispositivos operem de maneira segura e eficiente.
</p>

<p style="text-align:justify;">
A corrente de carregamento para uma bateria de chumbo-ácido é geralmente expressa como uma fração da capacidade nominal da bateria, conhecida como "C", que representa a capacidade da bateria em ampere-horas (Ah). 
</p>

<p style="text-align:justify;">
Para uma bateria de 14 Ah, a corrente de carregamento é determinada usando valores seguros típicos entre 0,1C e 0,3C.
</p>

### 6.1. Cálculo da Corrente de Carregamento
<p style="text-align:justify;">
A soma das capacidades das baterias é de 14 Ah, a taxa de carga segura para baterias de chumbo-ácido é tipicamente entre 0,1C e 0,3C. Então, para uma bateria de 14 Ah, temos que:
</p>

- 0,1C (corrente de carga baixa): 0,1 x 14 Ah = 1,4 A
- 0,3C (corrente de carga alta): 0,3 x 14 Ah = 4,2 A

<p style="text-align:justify;">
Portanto, a corrente de carregamento ideal para uma bateria de 12 V e 14 Ah deve estar na faixa de 1,4 A a 4,2 A.
</p>

<p style="text-align:justify;">
O carregador que será utilizado no projeto apresenta uma corrente de 3,215 A, encontrando-se dentro da faixa anteriormente calculada.
</p>

![Carregador](../assets/eletronica-energia/ACDC.jpg)

<font size="2"><p style="text-align: center">Figura 8 - Carregador da bateria A4514_DSM.</p></font>

### 6.2. Adaptação do Carregador

<p style="text-align:justify;">
A fim de evitar improvisações inadequadas, realizou-se uma modificação na extremidade da fonte. Primeiramente, a ponta original do carregador foi cortada. Em seguida, utilizando um multímetro, foram identificados e separados os terminais positivo e negativo. Posteriormente, a ponta antiga foi substituída por um plugue P4 macho. As conexões dos terminais positivo e negativo do carregador foram realizadas no plugue P4 de acordo com as especificações fornecidas pelo fabricante. Essa adaptação garantiu uma conexão mais segura para o carregamento eficiente das baterias.
</p>

## 7. Interruptores
<p style="text-align:justify;">
Para o controle do fornecimento de energia para o subsistema de eletrônica, foram utilizados dois botões interruptores, um para cada bateria. Isso permite fornecer ou interromper a energia conforme a necessidade do momento, visando maior praticidade e evitando o desperdício de energia das baterias e o superaquecimento dos componentes eletrônicos.
</p>

Especificações do Produto:
- Dimensões (Comprimento x Altura x Largura): 20 mm x 20 mm x 20 mm
- Corrente Nominal: 10 A
- Materiais: Plástico e Metal
- Quantidade de Canais: 2


![Botão liga-desliga](../assets/eletronica-energia/minibotao.png)

<font size="2"><p style="text-align: center">Figura 9 - Interruptor "liga-desliga".</p></font>

<p style="text-align:justify;">
Esse interruptor permite o controle eficiente da energia fornecida ao subsistema de eletrônica, garantindo o gerenciamento conforme a demanda operacional.
</p>

## 8. Diagrama Unifilar

<p style="text-align:justify;">
A Figura 10 apresenta o diagrama unifilar do projeto, com as características de fluxo de energia, no qual cada componente está identificado com suas respectivas características elétricas, como também as características da bitola do cabo.
</p>

![Dia_Unifilar](../assets/eletronica-energia/Diagrama_unifilar_12_JUL.jpg)

<font size="2"><p style="text-align: center">Figura 10 - Diagrama unifilar.</p></font>

## 9. Montagem

<p style="text-align:justify;">
Antes de iniciar o processo de montagem do subsistema de energia, foi desenvolvido pela equipe um diagrama de montagem que foi revisado e aprovado pelo docente responsável. Também foram revisitadas as normas técnicas relacionadas à solução.
</p>

<p style="text-align:justify;">
Após aprovação do diagrama e levantamento dos componentes, foi realizada a compra desses materiais e, posteriormente, iniciou-se a montagem do sistema no Laboratório de Eletricidade com o auxílio do técnico responsável.
</p>

<p style="text-align:justify;">
O sistema foi montado por partes. Inicialmente, as baterias foram testadas para garantir o fornecimento de 12 V. Em seguida, foram adicionados os barramentos e demais cabeamentos de acordo com a disposição do diagrama unifilar e a NBR 5410.
</p>

<p style="text-align:justify;">
Além disso, foram instalados os plugues P4 para facilitar a conexão entre o carregador e as baterias. O plugue P4 macho foi fixado na ponta do carregador, enquanto o plugue P4 fêmea foi instalado na estrutura de montagem. Os terminais positivo e negativo do plugue fêmea foram conectados aos barramentos respectivos, obedecendo as cores dos cabos (preto para negativo e vermelho para positivo).
</p>

<p style="text-align:justify;">
Os botões "liga-desliga" também foram fixados na estrutura. A lógica utilizada para a sua instalação foi a seguinte: a saída positiva da bateria foi conectada a um dos canais do botão, e o outro canal foi conectado ao barramento positivo. Dessa forma, quando o botão está desligado, não há fluxo de energia das baterias para o sistema. Esse procedimento foi realizado separadamente para cada botão e sua respectiva bateria.
</p>

<p style="text-align:justify;">
Com auxílio de um multímetro, foi aferida a tensão em todas as conexões entre os componentes, garantindo assim o bom funcionamento do subsistema e a qualidade de fornecimento de energia para os demais subsistemas.
</p>


## Referências Bibliográficas

[1] Associação Brasileira de Normas Técnicas (ABNT). NBR 5410: Instalações elétricas de baixa tensão. Rio de Janeiro, 2004.

<div id="ref-2"/>
[2] Instituto Nacional de Meteorologia (INMET): Clima. Prognóstico de Tempo (2024). Acesso em: 27 abr. 2024. Disponível em: https://clima.inmet.gov.br/progt.

[3] ABNT NBR IEC 60335-2-29:2010. Aparelhos eletrodomésticos e similares - Segurança - Parte 2-29: Requisitos particulares para carregadores de baterias. Rio de Janeiro: ABNT, 2010.

## Tabela de versionamento

| Versão| Data | Descrição | Responsável|
|-------|------|-----------|------------|
| 1.0 | 28/04/2024 | Criação do documento | Lucas Pantoja |
| 1.1 | 29/04/2024 | Edição de textos, tabelas e imagens | Lucas Pantoja |
| 1.2 | 01/05/2024 | Modificação da tabela 1 | Lucas Pantoja |
| 1.3 | 03/05/2024 | Modificação de texto para padronização | Lucas Pantoja |
| 1.4 | 03/05/2024 | Revisão da Diretoria Técnica | Carolina |
| 1.5 | 04/05/2024 | Ajustes de alinhamento e fontes | Ana Carolina |
| 1.6 | 02/06/2024 | Modificação de imagens e inserção de tópicos (LM317 e barramento) | Lucas Pantoja |
| 1.7 | 03/06/2024 | Revisão dos novos tópicos e inserção do tópico "Montagem" | Carolina |
| 1.8 | 06/06/2024 | Adição da imagem da montagem | Lucas Pantoja |
| 1.9 | 07/06/2024 | Criação do tópico "Carregador da Bateria" | Lucas Pantoja |
| 2.0 | 07/06/2024 | Revisão final para o PC 2 | Carolina |
| 2.1 | 12/07/2024 | Adequação do texto para a entrega final | Carolina |
| 2.2 | 12/07/2024 | Modificação do Power Budget e Diagrama Unifilar atualizado | Lucas Pantoja |


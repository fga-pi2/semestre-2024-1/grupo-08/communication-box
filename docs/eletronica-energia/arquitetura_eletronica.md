# Arquitetura do Subsistema Eletrônico

O desenvolvimento do subsistema eletrônico é essencial para garantir o controle preciso e eficiente do sistema, possibilitando o funcionamento ótimo do scanner e da mesa giratória. A seleção e integração adequadas dos componentes eletrônicos são cruciais para atender aos requisitos funcionais e não funcionais previamente estabelecidos.

## Componentes Principais

A arquitetura do subsistema é composta por motores de passo, microcontroladores Arduino Uno, a ponte H (L298N) e um regulador de tensão LM2596. Os motores de passo são responsáveis pelo controle da rotação da mesa e ajuste da altura da câmera. Os microcontroladores Arduino Uno processam os dados de escaneamento e gerenciam o funcionamento dos motores. A ponte H (L298N) permite a comunicação entre os motores e os Arduinos, facilitando o controle bidirecional necessário para a operação.

## Alimentação

O sistema é alimentado por uma bateria de 12V. O regulador de tensão LM2596 é utilizado para ajustar a tensão para 5V, necessário para alimentar os componentes eletrônicos, garantindo a estabilidade e eficiência do funcionamento do sistema.

## Simulações e Diagramas

Durante o desenvolvimento, simulações dos circuitos serão realizadas utilizando ferramentas como Proteus e Fritzing. Estas simulações, juntamente com diagramas de blocos e diagramas de barramento, oferecerão uma visão detalhada da arquitetura do subsistema eletrônico, facilitando a análise e validação do projeto.

## Versionamento

| Versão | Data       | Modificação                                  | Autor                           |
|--------|------------|----------------------------------------------|---------------------------------|
| 0.1    | 03/05/2024 | Documento inicial criado.                    | Carolina Oliveira, Miguel Munoz |
| 0.2    | 03/05/2024 | Inclusão do controle de versões.             | Ciro Costa                      |
| 0.3    | 04/05/2024 | Melhoria na formatação do documento.         | Ana Carolina                    |
| 0.4    | 04/05/2024 | Atualização e padronização da formatação.    | Carolina                        |
| 1.0    | 07/06/2024 | Revisão da arquitetura com base no feedback. | Carolina                        |
| 1.1    | 12/07/2024 | Ajustes finais do arquivo.                   | Pedro Menezes                   |
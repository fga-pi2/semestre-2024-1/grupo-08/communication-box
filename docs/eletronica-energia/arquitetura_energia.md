# Arquitetura do Subsistema de Energia

A arquitetura do subsistema de energia é fundamental para garantir a alimentação elétrica adequada e eficiente do produto ScanPoint. Este subsistema é projetado para operar em corrente contínua, utilizando duas baterias de chumbo-ácido que fornecem a energia necessária para os componentes eletrônicos, incluindo um microcontrolador, um sensor de infravermelho e dois módulos L298N. Os motores, essenciais para a rotação da mesa e a movimentação do sensor de infravermelho, são alimentados indiretamente através dos módulos L298N. A seguir, apresentamos o diagrama que ilustra a arquitetura de energia do sistema.

![Diagrama de Energia](../assets/eletronica-energia/Arquitetura_de_Subs_Energia.jpeg)

_Figura 1 - Arquitetura de Energia. Fonte: Autoria própria [1]._

## Versionamento

| Versão | Data       | Descrição                                   | Responsável   |
|--------|------------|---------------------------------------------|---------------|
| 1.0    | 28/04/2024 | Documento inicial criado.                   | Lucas Pantoja |
| 1.1    | 29/04/2024 | Revisão do texto e atualização da imagem.   | Lucas Pantoja |
| 1.2    | 03/05/2024 | Atualização para padronização do documento. | Lucas Pantoja |
| 1.3    | 04/05/2024 | Ajustes de alinhamento e formatação.        | Ana Carolina  |
| 1.4    | 12/07/2024 | Ajustes finais do arquivo.                  | Pedro Menezes |
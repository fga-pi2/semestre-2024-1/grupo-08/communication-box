<div>
  <center>
  <img class="photo" src="assets/scanpoint-logo.png" alt="logo">
  <h3 style="color: #000000; text-align: center">

<p style="text-align:justify;">No mundo contemporâneo, a integração entre dispositivos inteligentes e processos automatizados tem revolucionado diversos setores, impulsionando a eficiência e a produtividade. Nesse contexto, surge o projeto ScanPoint como uma resposta inovadora para simplificar e aprimorar o processo de digitalização 3D e reprodução tridimensional de objetos físicos.</p>
<p style="text-align:justify;">O ScanPoint é composto por uma mesa escaner e um aplicativo desktop. A mesa scaner foi desenvolvida com um Arduino e sensores infravermelhos, capazes de realizar leituras de objetos em tempo real. Esses dados são enviados para o aplicativo desktop, que permite o acompanhamento do processamento dos pontos capturados e serve de interface para a disponibilização do arquivo em formato STL e seu download. O processo de modelagem envolve a captura e processamento de dados para criar um modelo virtual o mais preciso possível. Esses modelos podem ser salvos e exibidos para o usuário, permitindo que reinicie o processo caso não esteja satisfeito antes de realizar o download.</p>
<p style="text-align:justify;">Essa solução pode ser aplicada em diversos setores, como manufatura, engenharia, design de produtos e educação. A aceleração da produção é uma das principais vantagens, permitindo que modelos 3D sejam desenvolvidos rapidamente a partir de leituras instantâneas, aumentando a precisão, reduzindo custos e o tempo de produção. Por exemplo, na engenharia, o sistema pode ser utilizado para criar protótipos e peças com maior rapidez e precisão.</p>
<p style="text-align:justify;">A adaptabilidade e versatilidade do ScanPoint são características fundamentais. O sistema é projetado para se ajustar a diferentes ambientes, desde residências até laboratórios e fábricas, tornando-o uma solução escalável e eficiente. Desenvolver um scanner 3D funcional a partir de componentes básicos, como um Arduino e sensores infravermelhos, apresenta desafios técnicos significativos, especialmente em termos de implementação do software e de calibração e funcionamento da estrutura, que podem afetar diretamente a qualidade/precisão da modelagem. Contudo, o projeto está focado em superar esses desafios para garantir a funcionalidade e a confiabilidade do sistema.</p>
<p style="text-align:justify;">Além disso, há uma preocupação com o impacto ambiental e a sustentabilidade do processo de fabricação digital. O ScanPoint visa otimizar o uso de materiais e reduzir o desperdício através de algumas estratégias. O sistema permite a reutilização das peças utilizadas e, unido ao design modular, facilita a atualização e substituição de componentes individuais, prolongando a vida útil do dispositivo e reduzindo a necessidade de descarte total.</p>
<p style="text-align:justify;">Para promover ainda mais a sustentabilidade, o ScanPoint pode ser compatível com materiais de impressão 3D ecológicos, como plásticos biodegradáveis ou reciclados, pois algumas das peças utilizadas na montagem da estruturas foram criadas a partir de impressões. Além disso, o projeto pode incluir iniciativas de reciclagem para o descarte adequado de componentes eletrônicos ao final de sua vida útil. Essas práticas contribuem para uma produção digital mais sustentável e ambientalmente responsável, alinhando-se com as melhores práticas de conservação ambiental e eficiência de recursos.</p>
  </h3>
</div>

<div>
<h1 style="color: #000000; font-weight: bold; text-align: center"> Documentações e Código fonte </h1>
<div class="pictures">
<a href="https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-08/scanpoint/-/tree/main/docs?ref_type=heads">
  <div class="repo-border">
	<img class="photoRepo" src="https://cdn-icons-png.flaticon.com/512/25/25231.png" alt="Geral">
  </div>
	<h4 class="legenda">Geral</h4>
	<h6 class=legenda>Relatórios gerais</h6>
</a>
<a href="https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-08/scanpoint/-/tree/main/docs/software?ref_type=heads">
  <div class="repo-border">
	<img class="photoRepo" src="https://cdn-icons-png.flaticon.com/512/25/25231.png" alt="Software">
  </div>
	<h4 class="legenda">Software</h4>
	<h6 class=legenda>Documentos</h6>
</a>
<a href="https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-08/scanpoint/-/tree/main/docs/eletronica-energia?ref_type=heads">
  <div class="repo-border">
	<img class="photoRepo" src="https://cdn-icons-png.flaticon.com/512/25/25231.png" alt="Eletrônica e Energia">
  </div>
	<h4 class="legenda">Eletrônica/Energia</h4>
	<h6 class=legenda>Documentos</h6>
</a>
<a href="https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-08/scanpoint/-/tree/main/docs/estruturas?ref_type=heads">
  <div class="repo-border">
	<img class="photoRepo" src="https://cdn-icons-png.flaticon.com/512/25/25231.png" alt="Estruturas">
  </div>
	<h4 class="legenda">Estruturas</h4>
	<h6 class=legenda>Documentos e modelos</h6>
</a>
<a href="https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-08/scanpoint-app">
  <div class="repo-border">
	<img class="photoRepo" src="https://cdn-icons-png.flaticon.com/512/25/25231.png" alt="App">
  </div>
	<h4 class="legenda">ScanPoint App</h4>
	<h6 class=legenda>Código</h6>
</a>
</div>
</div>

<div>
<h1 style="color: #000000; font-weight: bold; text-align: center"> Equipe </h1>
<h2 style="color: #000000; text-align: center"> Software </h2>
<div class="pictures">
<a class="pessoa" href="https://gitlab.com/brendavsantos">
  <div class="photo-border">
    <img class="photo" src="assets/membros/brenda.jpeg" alt="Brenda">
  </div>
  <h4 class="legenda">Brenda Santos</h4>
</a>
<a class="pessoa" href="https://gitlab.com/dennielwilliam">
  <div class="photo-border">
    <img class="photo" src="assets/membros/denniel.jpeg" alt="Denniel">
  </div class="container-legenda" >
  <h4 class="legenda">Denniel William</h4>
</a>
<a class="pessoa" href="https://gitlab.com/Carlacangussu">
  <div class="photo-border">
    <img class="photo" src="assets/membros/Carla.JPG" alt="Carla">
  </div>
  <h4 class="legenda">Carla Rocha</h4>
</a>
<a class="pessoa" href="https://gitlab.com/pemiinem">
  <div class="photo-border">
    <img class="photo" src="assets/membros/pedro.png" alt="Pedro">
  </div>
  <h4 class="legenda">Pedro Menezes</h4>
</a>
<a class="pessoa" href="https://gitlab.com/AnaCarolinaRodriguesLeite">
  <div class="photo-border">
    <img class="photo" src="assets/membros/Ana.jpg" alt="Ana">
  </div>
  <h4 class="legenda">Ana Carolina</h4>
</a>
<a class="pessoa" href="https://gitlab.com/GuilhermeBES">
  <div class="photo-border">
    <img class="photo" src="assets/membros/Guilherme.jpg" alt="Guilherme">
  </div>
  <h4 class="legenda">Guilherme Basílio</h4>
</a>
<a class="pessoa" href="https://gitlab.com/art_42">
  <div class="photo-border">
    <img class="photo" src="assets/membros/Artur.png" alt="Artur">
  </div>
  <h4 class="legenda">Artur de Souza</h4>
</a>
<a class="pessoa" href="https://gitlab.com/ciro-c">
  <div class="photo-border">
    <img class="photo" src="https://gitlab.com/uploads/-/system/user/avatar/6454030/avatar.png?width=800" alt="Ciro">
  </div>
  <h4 class="legenda">Ciro Costa</h4>
</a>
<a class="pessoa" href="https://gitlab.com/viniciusvieira00">
  <div class="photo-border">
    <img class="photo" src="assets/membros/vinicius.jpeg" alt="Vinicius">
  </div>
  <h4 class="legenda">Vinicius Vieira</h4>
</a>
</div>
</div>

<h2 style="color: #000000; text-align: center"> Energia </h2>
<div class="pictures">
<a class="pessoa" href="https://gitlab.com/carolinaroliveira02">
  <div class="photo-border">
    <img class="photo" src="assets/membros/Carolina.jpg" alt="Carolina">
  </div>
  <h4 class="legenda">Carolina Oliveira</h4>
</a>
<a class="pessoa" href="https://gitlab.com/lcs.pantoja.silva">
  <div class="photo-border">
    <img class="photo" src="assets/membros/Lucas.jpeg" alt="Lucas">
  </div>
  <h4 class="legenda">Lucas Pantoja</h4>
</a>
</div>

<h2 style="color: #000000; text-align: center"> Eletrônica </h2>
<div class="pictures">
<a class="pessoa" href="https://gitlab.com/migueleparra">
  <div class="photo-border">
    <img class="photo" src="assets/membros/Miguel.jpeg" alt="Miguel">
  </div>
  <h4 class="legenda">Miguel Munoz</h4>
</a>
</div>

<h2 style="color: #000000; text-align: center"> Aeroespacial </h2>
<div class="pictures">
<a class="pessoa" href="https://gitlab.com/mariaclaudialgaspar">
  <div class="photo-border">
    <img class="photo" src="assets/membros/MariaClaudia.jpeg" alt="Maria">
  </div>
  <h4 class="legenda">Maria Cláudia</h4>
</a>
<a class="pessoa" href="https://gitlab.com/Calcioft">
  <div class="photo-border">
    <img class="photo" src="assets/membros/Cassio.jpg" alt="Cássio">
  </div>
  <h4 class="legenda">Cássio Filho</h4>
</a>
</div>

<h2 style="color: #000000; text-align: center"> Automotiva </h2>
<div class="pictures">
<a class="pessoa" href="https://gitlab.com/soaressc321">
  <div class="photo-border">
    <img class="photo" src="assets/membros/diogo.jpeg" alt="Diogo">
  </div>
  <h4 class="legenda">Diogo Soares</h4>
</a>
</div>
</div>


# Protótipo de alta fidelidade

<p align="justify">
Este documento tem como objetivo apresentar uma representação detalhada do protótipo de alta fidelidade, que servirá como uma visualização tangível do produto final em relação ao software que será disponibilizado para interação com usuário. Ao proporcionar uma visão abrangente das interfaces e funcionalidades do ScanPoint, este documento visa garantir a compreensão clara e precisa de sua aparência e operação. Por meio do protótipo de alta fidelidade, os stakeholders terão a oportunidade de revisar e validar os elementos-chave do ScanPoint, contribuindo assim para o refinamento e aprimoramento contínuos do projeto.
</p>

[Link para o protótipo](https://www.figma.com/file/vCmfCgivle5xlXUYDElVrE/Identidade-visual?type=design&node-id=0%3A1&mode=design&t=DbKNrVOWZH43kGEs-1)

## Tela inicial

<p align="justify">
Telas desenhadas para fornecer uma breve introdução ao usuário sobre o produto e permitir que inicie o processo de escaneamento da forma correta.
</p>

![Tela inicial 1](../assets/software/prototipo/inicial-1.png)

<p style="text-align:center;">
Figura 1: Tela inicial 1<br />
Fonte: Autoria própria
</p>

![Tela inicial 2](../assets/software/prototipo/inicial-2.png)

<p style="text-align:center;">
Figura 2: Tela inicial 2<br />
Fonte: Autoria própria
</p>

## Tela de processamento

<p align="justify">
Tela idealizada para manter o usuário atualizado em relação ao tempo restante para finalizar o processamento e permitir que o processo seja interrompido ou re-iniciado, caso seja detectado algum problema no momento do processamento.
</p>

![Tela de processamento](../assets/software/prototipo/processamento.png)

<p style="text-align:center;">
Figura 3: Tela de processamento<br />
Fonte: Autoria própria
</p>

## Tela de erro

<p align="justify">
Tela desenhada para indicar que houve algum erro durante o processamento, indicando possíveis ações para que o processamento possa ser realizado apesar do erro e a causa provável. 
</p>

![Tela de erro](../assets/software/prototipo/erro.png)

<p style="text-align:center;">
Figura 4: Tela de erro <br />
Fonte: Autoria própria
</p>

## Tela de nuvem de pontos
Tela pensada para prover ao usuário a pré visualização do objeto de maneira tridimensional, para que ele possa ver como deve ficar no escaneamento 3D
![prototipo_nuvem](../assets/software/prototipo_nuvem.jpeg)

<p style="text-align:center;">
Figura 4: Tela de Nuvem de pontos<br />
Fonte: Autoria própria
</p>

## Tela de conclusão

<p align="justify">
Tela que informa que o processo foi concluído com sucesso. A tela mostra uma breve pré-visualização do esquema escaneado e permite que o usuário reinicie o processo, caso não esteja satisfeito com o resultado, baixe o arquivo resultante ou volte à tela inicial da aplicação.
</p>

![Tela de conclusão](../assets/software/prototipo/concluido.png)

<p style="text-align:center;">
Figura 5: Tela de conclusão <br />
Fonte: Autoria própria
</p>

## Sobre

<p align="justify">
Tela informativa sobre a idealização do projeto e sobre a equipe envolvida no desenvolvimento. 
</p>

![Tela de sobre](../assets/software/prototipo/sobre.png)

<p style="text-align:center;">
Figura 6: Tela de sobre <br />
Fonte: Autoria própria
</p>

## Tabela de versionamento

**Tabela 1:** Tabela de versionamento do documento Protótipo de alta fidelidade

| Versão| Data | Descrição | Responsável|
|-------|------|-----------|------------|
| 1.0 | 02/05/2024 | Criação do documento | Brenda Vitória |
| 1.1| 03/05/2024 | Refatoração do documento| Pedro Menezes Rodiguero |
| 1.2 | 03/05/2024 | Ajustando links | Brenda Santos |

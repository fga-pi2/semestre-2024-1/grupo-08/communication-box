# EAP Software

<p align="justify">
Este documento apresenta a Estrutura Analítica do Projeto (EAP) do ScanPoint para o subsistema de software. Aqui, são detalhadas e organizadas as entregas e atividades que compõem o desenvolvimento deste projeto inovador. 
</p>

<p align="justify"> A EAP serve como uma ferramenta fundamental para o planejamento e gerenciamento do projeto, oferecendo uma estrutura hierárquica que facilita a identificação e acompanhamento de cada elemento do projeto, desde os principais entregáveis até as tarefas mais específicas. 
</p>

<p align="justify"> Ao explorar este artefato, os envolvidos no projeto terão acesso a uma descrição mais esquemática dos componentes, incluindo sua relação com outros elementos e seu papel na consecução dos objetivos gerais.
</p>

## Resultados

<p align="center" size="2">
<figure>
  <iframe width="768" height="432" src="https://miro.com/app/board/uXjVKNvDGW8=/?share_link_id=568813235284" frameborder="0" scrolling="no" allow="fullscreen; clipboard-read; clipboard-write" allowfullscreen></iframe>
  <figcaption size="2" style="text-align: center">
  Figura 1: Estrutura Analítica do Projeto (EAP) do ScanPoint para o subsistema de software.
  </figcaption>
</figure>
</p>

<font size="2"><p style="text-align: center">    Fonte: Autoria própria.</p></font>


## Tabela de versionamento

| Versão| Data | Descrição | Responsável|
|-------|------|-----------|------------|
| 1.0 | 29/04/2024 | Criação do documento | Denniel William e Ana Carolina |
| 1.1 | 29/04/2024 | Adição da introdução | Brenda Vitória |
| 1.2 | 03/05/2024| Refatoração do documento| Pedro Menezes Rodiguero|
| 1.3 | 04/05/2024| Ajustes de alinhamento e fonte| Ana Carolina|